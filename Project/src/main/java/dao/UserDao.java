package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author takano
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      return new User(loginIdData, nameData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      // String sql = "SELECT * FROM user";
      String sql = "SELECT * FROM user  WHERE id!= 1 ";
      // "SELECT * FROM user WHERE name NOT IN 管理者 ";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);

      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public void useradd(String loginId, String password, String username, String birthdate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // →セレクトぶんを登録する文に書き換える
      // （）SQLの絡むと合わせる
      // ?とPreparedStatement pStmt の数を合わせる
      String sql =
          "INSERT INTO user (login_id, password,name,birth_date,is_admin,create_date,update_date)VALUES(?,?,?,?,false,now(),now())";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      pStmt.setString(3, username);
      pStmt.setString(4, birthdate);
      // 数合わせ
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public User userDetail(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE id = ?";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      // 検索した結果の時↓
      ResultSet rs = pStmt.executeQuery();

      // 探した結果がなかったら↓
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      // 検索結果があったら↓
      int Id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      // user型コンストラクタ
      return new User(Id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);// 上全部入れる

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void userupdate_pass(String id, String password, String username, String birthdate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          // SETno後ろは何個入れても○、WHEREのうしろどのデータを更新するのかの検索条件
          "UPDATE user SET password = ? ,name=?,birth_date=? WHERE id = ?;";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, password);
      pStmt.setString(2, username);
      pStmt.setString(3, birthdate);
      pStmt.setString(4, id);
      // 数合わせ
      pStmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void userupdate(String id, String username, String birthdate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql =
          // SETの後ろは何個入れても○、WHEREのうしろどのデータを更新するのかの検索条件
          "UPDATE user SET name=?,birth_date=? WHERE id = ?;";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, username);
      pStmt.setString(2, birthdate);
      pStmt.setString(3, id);
      // 数合わせ
      pStmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void userDelete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = " DELETE FROM user WHERE id = ?;";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      // SQLを検索しない時↓
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // 以下エラー処理
  public User findLoginId(String loginid) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE login_id = ?";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginid);
      // 検索した結果の時↓
      ResultSet rs = pStmt.executeQuery();

      // 探した結果がなかったら↓
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      // 検索結果があったら↓
      int Id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      // user型コンストラクタ
      return new User(Id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);// 上全部入れる

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // 検索結果が複数の場合List<User（変数）>
  // getParameterで受け取る時は必ずString。Stringに合わせる。
  public List<User> search(String login_id, String name, String endBirth,String startBirth) {
         Connection conn = null;    
         List<User> userList = new ArrayList<User>();
         try {
           // データベースへ接続
           conn = DBManager.getConnection();
           String sql =
               "SELECT * FROM user WHERE is_admin = false ";
               StringBuilder stringBuilder = new StringBuilder(sql);
             
              // "SELECT * FROM user WHERE is_admin = false AND login_id= ? AND name= ? AND birth_date= ?";
           // "SELECT * FROM user WHERE login_id= ? ";
           ArrayList<String> list = new ArrayList<String>();
           // if(じゃ無い時login_id.等しい(""))
           if(!login_id.equals("")) {
             stringBuilder.append("AND login_id= ?");
             // listは検索条件の値を入れる
             list.add(login_id);
             }
           if(!name.equals("")) {
             // 部分検索の場合は＝がlikeに置き換わる
             stringBuilder.append("AND name like ? ");
             list.add("%" + name + "%");
             }
            if(!startBirth.equals("")) {
              stringBuilder.append("AND birth_date>=?");
              list.add(startBirth);
            }
            if(!endBirth.equals("")) {
              stringBuilder.append("AND birth_date<=?");
              list.add(endBirth);
            }
            
           PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
           // ★★★ カウントをとる ★★★
           for (int i = 0; i < list.size(); i++) {
             // ★★★ 暗記★ArrayListの時のsetString ★★★
             pStmt.setString(i + 1, list.get(i));
           

           }

           ResultSet rs = pStmt.executeQuery();
           // ifは結果が一つの時。複数の時はwhile文
           while (rs.next()) {
             int id = rs.getInt("id");
             String loginId = rs.getString("login_id");
             String Username = rs.getString("name");
             Date birthDate = rs.getDate("birth_date");
             String password = rs.getString("password");
             boolean isAdmin = rs.getBoolean("is_admin");
             Timestamp createDate = rs.getTimestamp("create_date");
             Timestamp updateDate = rs.getTimestamp("update_date");
             User user =
                 new User(id, loginId, Username, birthDate, password, isAdmin, createDate, updateDate);

             userList.add(user);
           }
         } catch (SQLException e) {
           e.printStackTrace();
           return null;
         } finally {
           // データベース切断
           if (conn != null) {
             try {
               conn.close();
             } catch (SQLException e) {
               e.printStackTrace();
               return null;
             }
           }
         }
         return userList;
       }
     }
