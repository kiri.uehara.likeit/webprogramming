package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDetailServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // URLからGETパラメータとしてIDを受け取る
    String id = request.getParameter("id");

    // 確認用：idをコンソールに出力
    System.out.println(id);
    // ログインしてなかった時の処理。DAOに行く前
    HttpSession session = request.getSession();
    User loginuser = (User) session.getAttribute("userInfo");
    if (loginuser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    // ↑ここまで
    // TODO 未実装：idを引数にして、idに紐づくユーザ情報を出力する

    UserDao userDao = new UserDao();
    User user = userDao.userDetail(id);

    // TODO 未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
    request.setAttribute("user", user);
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
    dispatcher.forward(request, response);

  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {


  }

}
