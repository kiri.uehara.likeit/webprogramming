package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import utill.PasswordEncorder;

/**
 * Servlet implementation class Useradd
 */
@WebServlet("/UseraddServlet")
public class UseraddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UseraddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      HttpSession session = request.getSession();
      User loginuser = (User) session.getAttribute("userInfo");

      if (loginuser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String passwordconfirm = request.getParameter("password-confirm");
      String username = request.getParameter("user-name");
      String birthdate = request.getParameter("birth-date");

      UserDao userDao = new UserDao();
      // returnがある時は変数○＝の形。ないよきはない。
      User user = userDao.findLoginId(loginId);

      // ↓IF(）⇦復習標準クラスのスライド
      if (loginId.equals("") || password.equals("") || passwordconfirm.equals("")
          || username.equals("") || birthdate.equals("")) {
        request.setAttribute("errMsg", "入力された内容は正しくありません。");
        /*
         * 04.Webアプリケーションの開発(基礎) request.setAttribute("key(鍵)となる文字列", 実際に追加したいデータ(インスタンス形式));
         * →★★(value="${持ってきたいここのデータ}",String 持ってきたいここのデータ = request.getParameter("birth-date");)★★
         * エラーの時残すやつ★class="form-control" value=””
         */
        request.setAttribute("loginId", loginId);
        request.setAttribute("username", username);
        request.setAttribute("birthdate", birthdate);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }
      if (!password.equals(passwordconfirm)) {
        request.setAttribute("errMsg", "入力された内容は正しくありません。");
        request.setAttribute("loginId", loginId);
        request.setAttribute("username", username);
        request.setAttribute("birthdate", birthdate);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;

      }
      if (user != null) {
        request.setAttribute("errMsg", "入力された内容は正しくありません。");
        request.setAttribute("loginId", loginId);
        request.setAttribute("username", username);
        request.setAttribute("birthdate", birthdate);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }


      // ダオに暗号化は登録する前にする
      String encodestr = PasswordEncorder.encordPassword(password);

      // 登録
      userDao.useradd(loginId, encodestr, username, birthdate);



      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");

	}

  }
