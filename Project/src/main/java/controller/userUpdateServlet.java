package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import utill.PasswordEncorder;

/**
 * Servlet implementation class userUpdateServlet
 */
@WebServlet("/userUpdateServlet")
public class userUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public userUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {


    String id = request.getParameter("id");
    System.out.println(id);

    HttpSession session = request.getSession();
    User loginuser = (User) session.getAttribute("userInfo");

    if (loginuser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    UserDao userDao = new UserDao();
    User user = userDao.userDetail(id);

    request.setAttribute("user", user);
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);


  }


  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    // ★("")→jsp内のinputタグ内のname=と紐付く★
    // stringの後ろは変数
    String id = request.getParameter("user-id");
    String username = request.getParameter("user-name");
    String birthdate = request.getParameter("birth-date");
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String passwordconfirm = request.getParameter("password-confirm");


    if (id.equals("") || loginId.equals("") || username.equals("") || birthdate.equals("")
        || !password.equals(passwordconfirm)) {
      request.setAttribute("errMsg", "入力された内容は正しくありません。");
      User user = new User();
      user.setName(username);
      user.setLoginId(loginId);

      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      Date date;
      try {
        date = dateFormat.parse(birthdate);
      } catch (ParseException e) {
        // e.printStackTrace();
        date = null;
      }
      user.setBirthDate(date);

      request.setAttribute("user", user);
      // request.setAttribute("user.id", id);
      // request.setAttribute("user.loginId", loginId);
      // request.setAttribute("user.name", username);
      // request.setAttribute("user.birthDatee", birthdate);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;
    }

    String encodestr = PasswordEncorder.encordPassword(password);

    UserDao userDao = new UserDao();
    if (!password.equals("")) {
      userDao.userupdate_pass(id, encodestr, username, birthdate);
    } else {
      userDao.userupdate(id, username, birthdate);
    }



    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");

  }

  /*
   * User user = new User(); user.setName(username); user.setLoginId(loginId);
   * user.setBirthDate.format(birthdate);
   */
  // user.setBirthDate(birthdate)toString();
}
